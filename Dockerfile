FROM alpine:latest
COPY both.c both.c
RUN apk add build-base
RUN gcc -Wall both.c -o /usr/local/bin/both
CMD ["both"]
