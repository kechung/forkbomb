# forkbomb

__**Warning: This is meant to bring your hardware to a crawl. Do not use on a production system.**__

Use buildah/podman/docker to create an image based off of this Dockerfile. The default method is to compile the 'both.c' file into a binary inside the container, and run it.

For example: 
```
buildah build-using-dockerfile -t forkbomb:latest && podman run --name forkbomb --rm localhost/forkbomb:latest
```

If you want to test only CPU or Memory alone, modify the Dockerfile to change the source file it compiles.

